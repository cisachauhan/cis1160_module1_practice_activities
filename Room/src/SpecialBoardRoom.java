import java.util.Scanner;

/**
 * This class holds the information and functionality related to a special board
 * room.
 *
 * @author Ashley Patterson
 * @since 20150327
 */
public class SpecialBoardRoom extends BoardRoom {

    private int numberOfRecliners;
    private boolean hasCoffeeMaker;

    public SpecialBoardRoom(int roomNumber) {
        super(roomNumber);
    }

    /**
     * Get the details from the user about this class. This will invoke the
     * super method to get the base class attributes.
     *
     * @author BJ MacLean, modified by Ashley Patterson
     * @since 20150327
     */
    @Override
    public void getRoomDetailsFromUser() {
        super.getRoomDetailsFromUser();
        Scanner input = new Scanner(System.in);
        System.out.println("How many recliners does this room have?");
        numberOfRecliners = Integer.parseInt(input.nextLine());
        System.out.print("Does this room have a coffee maker? (Y/N):");
        hasCoffeeMaker = input.nextLine().equalsIgnoreCase("y");

    }

    @Override
    public String toString() {
        return super.toString() + "\nNumber of Recliners: " + numberOfRecliners
                + "\nHas a Coffee Maker: " + hasCoffeeMaker;
    }
}
