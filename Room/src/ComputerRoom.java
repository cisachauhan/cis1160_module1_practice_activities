import java.util.Scanner;

/**
 * This class holds the information and functionality related to a computer
 * room.
 *
 * @author BJ MacLean
 * @since 20150325
 */
public class ComputerRoom extends Room {

    int numberOfComputers;

    public ComputerRoom(int roomNumber) {
        super(roomNumber);
    }

    /**
     * Get the details from the user about this class. This will invoke the
     * super method to get the base class attributes.
     *
     * @author BJ MacLean
     * @since 20150325
     */
    public void getRoomDetailsFromUser() {
        super.getRoomDetailsFromUser();
        Scanner input = new Scanner(System.in);
        System.out.print("Enter number of computers: ");
        numberOfComputers = input.nextInt();
        input.nextLine();
    }

    public int getNumberOfComputer() {
        return numberOfComputers;
    }

    public void setNumberOfComputer(int numberOfComputer) {
        this.numberOfComputers = numberOfComputer;
    }

    @Override
    public String toString() {
        return super.toString() + "\nNumber of Computers: " + numberOfComputers;
    }
}
