import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**
 * This main class will invoke functionality for booking college rooms.
 *
 * Detailed description:
 * https://docs.google.com/document/d/1jyrvSJHXS6BZuXKVswYkmt2muBmPI71OxXTLQxerDVU/edit
 *
 * @author cis1232, modified by Ashley Patterson
 * @since 20150327
 */
public class RoomMain {

    private static final int ROOM_DOES_NOT_EXIST = -1;
    private static ArrayList<Room> rooms = new ArrayList();

    /**
     * Main method controls program and user interface.
     *
     * @param args the command line arguments
     * @author BJ MacLean, modified by Ashley Patterson
     * @since 20150327
     */
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        String menu = "******************************"+System.lineSeparator()+"Choose an option:\n"
                + "1) Add Room\n"
                + "2) Reserve Room\n"
                + "3) Release Room\n"
                + "4) Show Rooms\n"
                + "5) Search For Room\n"
                + "6) Display Room Count Details\n"
                + "7) Exit";
        int selection = 0;

        while (selection != 7) {
            System.out.println(menu);
            selection = input.nextInt();
            input.nextLine();
            switch (selection) {
                case 1:
                    addRoom();
                    break;
                case 2:
                    reserveRoom();
                    break;
                case 3:
                    releaseRoom();
                    break;
                case 4:
                    showRooms();
                    break;
                case 5:
                    RoomUtility.findRoom(rooms);
                    break;
                case 6:
                    RoomUtility.roomSummary(rooms);
                    break;
                case 7:
                    break;
                default:
                    System.out.println("Invalid option.");
            }
        }

    }

    /**
     * Loop through the rooms to check if the room already exists.
     *
     * @author BJ MacLean
     * @since 20150325
     *
     * @param roomNumber
     * @return the index of the room number
     */
    public static int getRoomNumberIfExists(int roomNumber) {
        int index = -1;
        for (int i = 0; i < rooms.size(); i++) {
            if (rooms.get(i).getRoomNumber() == roomNumber) {
                index = i;
            }
        }
        return index;
    }

    /**
     * This method will allow the user to add a new room to the collection of
     * rooms.
     *
     * @author BJ MacLean, modified by Ashley Patterson
     * @since 20150329
     */
    public static void addRoom() {

        //Ask which room number the user wants to add
        Room room = null;
        Scanner input = new Scanner(System.in);
        System.out.print("Enter room number: ");
        int roomNumber = input.nextInt();
        input.nextLine();

        //Check to see if the room already exists
        int roomNumberIndex = getRoomNumberIfExists(roomNumber);

        //If the room does not already exist.
        if (roomNumberIndex == ROOM_DOES_NOT_EXIST) {
            roomNumberIndex = rooms.size();
            boolean finished = false;
            do {
                System.out.print("What type of room is this?\n" + "1) Basic Room\n"
                        + "2) Computer Lab\n"
                        + "3) Board Room\n"
                        + "4) Biology Lab\n");
                String choice = input.nextLine();

                //Based on the user input, create the correct type of room.  
                switch (choice) {
                    case "1":
                        room = new Room(roomNumber);
                        finished = true;
                        break;
                    case "2":
                        room = new ComputerRoom(roomNumber);
                        finished = true;
                        break;
                    case "3":
                        System.out.println("Is this a special boardroom (with coffeemaker/recliners)? (Y/N)");
                        String special = input.nextLine();
                        if (special.equalsIgnoreCase("y")) {
                            room = new SpecialBoardRoom(roomNumber);
                        } else {
                            room = new BoardRoom(roomNumber);
                        }
                        finished = true;
                        break;
                    case "4":
                        room = new BiologyLab(roomNumber);
                        finished = true;
                        break;
                    default:
                        System.out.println("Invalid option");

                }
            } while (!finished);

            //Set the details for the room
            room.getRoomDetailsFromUser();

            //Add the room to the collection of rooms.  
            rooms.add(room);

        } else {
            String choice = "";
            System.out.println("Room already exists. Do you want to continue? (Y/N)");
            choice = input.nextLine();

            //If the user wants to continue, invoke the method to change the value of attributes in 
            //the room
            if (choice.equalsIgnoreCase("y")) {
                rooms.get(roomNumberIndex).getRoomDetailsFromUser();
            }
        }
    }

    /**
     * This method will allow the user to reserve a room.
     *
     * @author BJ MacLean
     * @since 20150325
     */
    public static void reserveRoom() {

        Scanner input = new Scanner(System.in);
        System.out.println("Enter the room number you would like to book");
        int roomNumber = input.nextInt();
        input.nextLine();

        //Check to see if the room exists.
        int roomNumberIndex = getRoomNumberIfExists(roomNumber);
        if (roomNumberIndex < 0) {
            System.out.println("This room does not exist");
        } else {
            //Put the room from the ArrayList into a local variable.
            Room room = rooms.get(roomNumberIndex);
            if (!room.isReserved()) {
                room.reserveThisRoom();
            } else {
                System.out.println("This room is already booked!");
            }
        }
    }

    /**
     * This method will allow the user to release a reservation on a room.
     *
     * @author BJ MacLean
     * @since 20150325
     */
    public static void releaseRoom() {

        Scanner input = new Scanner(System.in);
        System.out.println("Enter the room number you would like to release");
        int roomNumber = input.nextInt();
        input.nextLine();

        //Check if the room exists.  
        int roomNumberIndex = getRoomNumberIfExists(roomNumber);

        if (roomNumberIndex < 0) {
            System.out.println("This room does not exist");
        } else {
            //Put the room from the ArrayList into a local variable.
            Room room = rooms.get(roomNumberIndex);
            //If the room is reserved, allow them to release.
            if (room.isReserved()) {
                room.releaseThisRoom();
            } else {
                System.out.println("This room is not booked!");
            }
        }
    }

    /**
     * Show the details for each room
     *
     * @author BJ MacLean
     * @since 20150325
     */
    public static void showRooms() {
        
        Collections.sort(rooms);
        
        for (int i = 0; i < rooms.size(); i++) {
            System.out.println(rooms.get(i));
        }
    }
}
