# Creating Repos as a team


- [x] Someone create 1 new repo in Bitbucket
- [x] Give everyone in the group write access
- [x] Everyone clone the repo to their local machine --arvind updated this after clone and pull
- [x] Everyone add files to the repo and stage, commit and push changes to Bitbucket
- [x] Everyone do a pull to get the latest changes from Bitbucket repo
- [x] Everyone change a file that was not theirs and stage, commit and push back to Bitbucket
- [x] Everyone pull latest changes
- [x] Everyone change the same file and stage, commit and push back to Bitbucket 

This can be the file the tracks the group progress. This could be the file we all edit as well. Add whatever information you want that makes sense.

We can each create our own files to track our own progress. 

** Done** Sept 25 2018 6:05 pm CST Arvind
