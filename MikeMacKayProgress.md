# Progress of Task by Mike MacKay

- [ ] Someone create 1 new repo in Bitbucket
- [ ] Give everyone in the group write access
- [x] Everyone clone the repo to their local machine
- [x] Everyone add files to the repo and stage, commit and push changes to Bitbucket
- [x] Everyone do a pull to get the latest changes from Bitbucket repo
- [x] Everyone change a file that was not theirs and stage, commit and push back to Bitbucket
- [x] Everyone pull latest changes		-- (Sept 25 2018 5:57 pm CST) Arvind updated this record for last step for group activity 1.1
- [X] Everyone change the same file and stage, commit and push back to Bitbucket

I will watch the repo for your updates to procede with my task.


NOTE: This file may not be formatted correctly on Bitbucket. This works nicely in GitHub (Github Flavored Markdown), but I am new to Bitbucket so we will see.

** (Sept 25 2018 5:57 pm CST)**lets make changes to this file and complete activity 1. There are 4  more to complete. Thanks Arvind

Sounds good.  I have updated this file and will push back to the repo. 
